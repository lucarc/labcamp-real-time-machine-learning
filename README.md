# Useful links from Kafka documentation

- Broker configs:
https://kafka.apache.org/documentation/#brokerconfigs

- Topic-Level configs:
https://kafka.apache.org/documentation/#topicconfigs

- Producer Configs:
https://kafka.apache.org/documentation/#producerconfigs

- Consumer Configs:
https://kafka.apache.org/documentation/#consumerconfigs


# Useful links from Kafka-Python library documentation

- Producer documentation:
https://kafka-python.readthedocs.io/en/master/apidoc/KafkaProducer.html

- Consumer documentation:
https://kafka-python.readthedocs.io/en/master/apidoc/KafkaConsumer.html


- Configuration fields for both Consumer and Producer (confluent-kafka library):
https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md